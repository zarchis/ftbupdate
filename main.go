package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/BurntSushi/toml"
)

//config server
type Config struct {
	Server ServerConfig
}

//config server-
type ServerConfig struct {
	RootURL  string `toml:"root_url"`
	SignKey  string `toml:"sign_key"`
	Interval int64  `toml:"interval"`
}

func loadConfig(dir string) (Config, error) {
	confPath := path.Join(dir, "crawl.conf")
	var conf Config
	_, err := toml.DecodeFile(confPath, &conf)
	return conf, err
}

func update(conf Config) {
	url := conf.Server.RootURL + "cron_get.php"
	resp, _ := http.Get(url)
	defer resp.Body.Close()

	scanner := bufio.NewScanner(resp.Body)

	for scanner.Scan() {
		text := scanner.Text()
		fmt.Println(text)
	}
}

func main() {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
		return
	}

	conf, err := loadConfig(dir)
	if err != nil {
		log.Fatal(err)
		return
	}

	for {
		fmt.Println("start update")
		update(conf)
		fmt.Println("done  update")
		fmt.Printf("wait %v second\n", conf.Server.Interval)
		time.Sleep(time.Second * time.Duration(conf.Server.Interval))
	}

}
