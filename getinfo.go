package getinfo

import (
	"encoding/json"
	"log"
	"net/http"
	"path"

	"github.com/syndtr/goleveldb/leveldb"
)

type ftbThreadInfo struct {
	Date        int    `json:"date"`
	Title       string `json:"title"`
	URL         string `json:"url"`
	StoreURL    string `json:"store_url"`
	Thumb       string `json:"thumb"`
	HeadRes     string `json:"res"`
	Lost        int    `json:"lost"`
	Du          int    `json:"du"`
	ResCount    int    `json:"res_count"`
	FirstUpdate int    `json:"first_date"`
	UserUpdate  int    `json:"user_update"`
	FileCount   int    `json:"other_files"`
	HTMLHash    string `json:"htmlhash"`
}

func readAllInfo(dir string, conf Config) []ftbThreadInfo {
	url := conf.Server.RootURL + "json_api.php?action=readallinfo"

	resp, _ := http.Get(url)
	defer resp.Body.Close()
	decoder := json.NewDecoder(resp.Body)
	var infoList []ftbThreadInfo
	decoder.Decode(&infoList)

	dbpath := path.Join(dir, "logger.db")
	db, err := leveldb.OpenFile(dbpath, nil)
	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}

	for _, info := range infoList {
		jsonBytes, err := json.Marshal(info)
		if err == nil {
			db.Put([]byte(info.URL), jsonBytes, nil)
		}
	}
	return infoList
}
